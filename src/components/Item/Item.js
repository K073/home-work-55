import React from 'react';
import './Item.css'

const Item = props => {
    const hidden = props.hidden ? {backgroundColor: "rgb(128, 128, 128)"} : {backgroundColor: "rgba(128, 128, 128, 0)"};
    const hasItem = props.hasItem && !props.hidden ? <i>O</i> : null;
    return <span className="Item" style={hidden} onClick={props.click}>{hasItem}</span>
};


export default Item;