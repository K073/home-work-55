import React, { Component } from 'react';
import './App.css';
import Desk from "./containers/Desk/Desk";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Desk/>
      </div>
    );
  }
}

export default App;
