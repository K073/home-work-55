import React, {Component} from 'react';
import Item from "../../components/Item/Item";
import './Desk.css'

class Desk extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arrItems: [],
            endGame: false
        };

        for (let i = 0; i < 36; i++) {
            let item = {
                hasItem: false,
                hidden: true
            };
            this.state.arrItems.push(item);
        }

        this.state.arrItems[Math.floor(Math.random() * this.state.arrItems.length)].hasItem = true;
    }

    resetDesk = () => {
        const arrItems = [];
        for (let i = 0; i < 36; i++) {
            let item = {
                hasItem: false,
                hidden: true
            };

            arrItems.push(item);
        }
        arrItems[Math.floor(Math.random() * this.state.arrItems.length)].hasItem = true;

        const endGame = false;

        this.setState({endGame});
        this.setState({arrItems});
    };

    showItem = (index) => {
        if (!this.state.endGame){
            const arrItems = [...this.state.arrItems];
            arrItems[index].hidden = false;

            let endGame = arrItems[index].hasItem;

            this.setState({endGame});
            this.setState({arrItems});
        }
    };

    render() {
        let counter = 0;
        this.state.arrItems.forEach((item) => {
            if (!item.hidden){
                counter++;
            }
        });
        return (
            <div className="Desk">
                {this.state.arrItems.map((value, index) => {
                    return <Item key={index} hidden={value.hidden} hasItem={value.hasItem} click={this.showItem.bind(this, index)}/>
                })}
                <div>Tries: {counter}</div>
                <button onClick={this.resetDesk.bind(this)}>reset</button>
            </div>
        );
    }
}

export default Desk;